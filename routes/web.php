<?php

use App\Http\Controllers\Admin\MessageController;
use App\Http\Controllers\Admin\Products\ComputerController;
use App\Http\Controllers\Admin\Products\EarphoneController;
use App\Http\Controllers\Admin\Products\TelephoneController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Livewire\Admin\Dashboard;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front Routes
Route::get('/', function(){
    return view('front.welcome');
})->name('home');

    // Admin Routes
    Route::middleware([
        'auth:sanctum',
        config('jetstream.auth_session'),
        'verified'
    ])->group(function () {
        Route::middleware(['is_admin'])->group(function(){
            Route::prefix('admin')->group(function () {
                Route::name('admin.')->group(function () {
                    Route::get('/dashboard', Dashboard::class)->name('dashboard');
                    Route::get('/users', [UserController::class, 'index'])->name('users');
                    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
                    Route::resource('computers',ComputerController::class);
                    Route::resource('telephones',TelephoneController::class);
                    Route::resource('earphones',EarphoneController::class);
                    Route::controller(MessageController::class)->group(function () {
                        Route::get('/notifications', 'notifications')->name('notifications');
                        Route::get('/notify/{id}', 'notify')->name('notify');
                        Route::get('/markasread/{id}', 'markAsRead')->name('markasread');
                        Route::get('/markallasread', 'markAllAsRead')->name('markallasread');
                    });

                });
            });
        });



    });


Route::fallback(function () {
    return view('admin.components.error404');
});
