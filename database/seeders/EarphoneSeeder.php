<?php

namespace Database\Seeders;

use App\Models\Earphone;
use Illuminate\Database\Seeder;

class EarphoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Earphone::factory(50)->create();
    }
}
