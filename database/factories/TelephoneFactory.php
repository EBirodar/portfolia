<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TelephoneFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'color'=>$this->faker->colorName(),
            'price'=>$this->faker->numberBetween(1,1000),
            'character'=>$this->faker->name(),
            'img'=>$this->faker->name(),
            'status'=>$this->faker->numberBetween(1,2)
        ];
    }
}
