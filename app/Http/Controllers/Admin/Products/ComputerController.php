<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Models\Computer;
use Illuminate\Http\Request;

class ComputerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
//     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd('a');
        $computers=Computer::orderByDesc('created_at')->paginate(5);

        return view('admin.products.computer.index',['products'=>$computers]);
    }

    /**
     * Show the form for creating a new resource.
     *
//     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.computer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
//     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Computer::create($this->validateData());

        return redirect()->route('admin.computers.index');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Computer  $computer
//     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $comp=Computer::find($id);
        return view('admin.products.computer.show',['product'=>$comp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
//     * @param  \App\Models\computer  $computer
//     * @return \Illuminate\Http\Response
     */
    public function edit(Computer $computer)
    {
        return view('admin.products.computer.edit',[
            'product'=>$computer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateComputerRequest  $request
     * @param  \App\Models\computer  $computer
//     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Computer $computer)
    {
        $computer->update($this->validateData());
        return redirect()->route('admin.computers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Computer  $computer
//     * @return \Illuminate\Http\Response
     */
    public function destroy(Computer $computer)
    {
//        dd('a');
        $computer->delete();
        return redirect()->route('admin.computers.index');
    }

    public function validateData()
    {
        return request()->validate([
            'name'=>'required',
            'color'=>'required',
            'img'=>'required',
            'character'=>'required',
            'price'=>'required',
            'status'=>'required'
        ]);

    }
}
