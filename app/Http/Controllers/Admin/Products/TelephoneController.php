<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Models\Telephone;
use Illuminate\Http\Request;

class TelephoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $telephones=Telephone::orderByDesc('created_at')->paginate(7);
        return view('telephone.index',['products'=>$telephones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('telephone.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        Telephone::create($this->validateData());
        return redirect()->route('telephones.index');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Telephone  $Telephone
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $comp=Telephone::find($id);
        return view('telephone.show',['product'=>$comp]);
    }

    /**
     * Show the form for editing the specified resource.
     *
    //     * @param  \App\Models\Telephone  $Telephone
    //     * @return \Illuminate\Http\Response
     */
    public function edit(Telephone $telephone)
    {
        return view('telephone.edit',[
            'product'=>$telephone
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTelephoneRequest  $request
     * @param  \App\Models\Telephone  $Telephone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Telephone $telephone)
    {
        $telephone->update($this->validateData());
        return redirect()->route('telephones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Telephone  $Telephone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Telephone $telephone)
    {
        $telephone->delete();
        return redirect()->route('telephones.index');
    }

    public function validateData()
    {
        return request()->validate([
            'name'=>'required',
            'color'=>'required',
            'img'=>'required',
            'character'=>'required',
            'price'=>'required',
            'status'=>'required'
        ]);

    }
}
