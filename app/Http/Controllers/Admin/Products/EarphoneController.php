<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEarphoneRequest;
use App\Http\Requests\UpdateEarphoneRequest;
use App\Models\earphone;

class EarphoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEarphoneRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEarphoneRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\earphone  $earphone
     * @return \Illuminate\Http\Response
     */
    public function show(earphone $earphone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\earphone  $earphone
     * @return \Illuminate\Http\Response
     */
    public function edit(earphone $earphone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEarphoneRequest  $request
     * @param  \App\Models\earphone  $earphone
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEarphoneRequest $request, earphone $earphone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\earphone  $earphone
     * @return \Illuminate\Http\Response
     */
    public function destroy(earphone $earphone)
    {
        //
    }
}
